package com.muzhongyun.codegenerator.config;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

/**
 * 默认的代码生成的配置
 *
 * @author HeYongSheng
 * @date 2019/05/17 14:39
 */
public class CodeGeneratorConfig {

	/**
	 * 写自己项目的绝对路径,注意具体到java目录
	 */
	private String outputDir = "D:\\IdeaProjects\\code-generator\\src\\main\\java";
	private String author = "ZhogKeRuan";
	private String[] tablePrefix = new String[] {""};
	private String[] tableList = new String[] {"analysis_model"};
	private String packagePath = "com.example.xrk.saas.modules";
	private String moduleName = "records";

	private DataSourceConfig dataSourceConfig() {
		DataSourceConfig dataSourceConfig = new DataSourceConfig();
		dataSourceConfig.setDbType(DbType.MYSQL);
		dataSourceConfig.setUrl("jdbc:mysql://47.97.116.33:3306/big_data_analysis_job?useUnicode=true&characterEncoding=utf-8&useSSL=false");
		dataSourceConfig.setUsername("root");
		dataSourceConfig.setPassword("Sinosoft77675998");
		 //mysql 5
		dataSourceConfig.setDriverName("com.mysql.jdbc.Driver");
		// mysql 8
		//dataSourceConfig.setDriverName("com.mysql.cj.jdbc.Driver");


		//dataSourceConfig.setDbType(DbType.ORACLE);
		//dataSourceConfig.setUrl("jdbc:oracle:thin:@192.168.0.114:1521:ORCL");
		//
		//dataSourceConfig.setUsername("EPLATFORM");
		//dataSourceConfig.setPassword("EPLATFORM");
		//dataSourceConfig.setDriverName("oracle.jdbc.driver.OracleDriver");

		return dataSourceConfig;
	}

	private GlobalConfig globalConfig() {
		GlobalConfig globalConfig = new GlobalConfig();
		globalConfig.setAuthor(this.author);
		globalConfig.setOutputDir(this.outputDir);
		globalConfig.setFileOverride(true);
		globalConfig.setActiveRecord(true);
		// 开启 BaseResultMap
		globalConfig.setBaseResultMap(true);
		// 开启 baseColumnList
		globalConfig.setBaseColumnList(false);
		//只使用 java.util.date 代替
		globalConfig.setDateType(DateType.ONLY_DATE);
		globalConfig.setIdType(IdType.ID_WORKER);

		// model swagger2
		globalConfig.setSwagger2(true);

		// 自定义文件命名，注意 %s 会自动填充表实体属性！
		globalConfig.setMapperName("%sMapper");
		globalConfig.setXmlName("%sMapper");
		globalConfig.setServiceName("%sService");
		globalConfig.setServiceImplName("%sServiceImpl");
		globalConfig.setControllerName("%sController");

		// 是否打开输出目录
		globalConfig.setOpen(false);

		return globalConfig;
	}

	private StrategyConfig strategyConfig() {
		StrategyConfig strategyConfig = new StrategyConfig();
		// 全局大写命名 ORACLE 注意
		//strategyConfig.setCapitalMode(true);

		// 此处可以修改为您的表前缀(数组)
		strategyConfig.setTablePrefix(this.tablePrefix);
		//修改替换成你需要的表名，多个表名传数组
		strategyConfig.setInclude(this.tableList);
		// 表名生成策略
		strategyConfig.setNaming(NamingStrategy.underline_to_camel);
		// 排除生成的表
		//strategyConfig.setExclude(new String[]{"test"});

		// lombok 实体
		strategyConfig.setEntityLombokModel(true);
		// 逻辑删除属性名称
		//strategyConfig.setLogicDeleteFieldName("status");

		return strategyConfig;
	}

	protected PackageConfig packageConfig() {
		// 包信息配置 多层级目录以.隔开
		PackageConfig packageConfig = new PackageConfig();
		packageConfig.setParent(this.packagePath);
		// controller目录
		packageConfig.setController("controller" + StrUtil.DOT + this.moduleName);
		// service目录
		packageConfig.setService("service" + StrUtil.DOT + this.moduleName);
		packageConfig.setServiceImpl("service.impl" + StrUtil.DOT + this.moduleName);
		// 实体类目录
		packageConfig.setEntity("model.domain" + StrUtil.DOT + this.moduleName);
		// mapper存放目录
		packageConfig.setMapper("mapper" + StrUtil.DOT + this.moduleName);
		// xml目录
		packageConfig.setXml("mapper.mapping" + StrUtil.DOT + this.moduleName);

		return packageConfig;
	}

	/**
	 * 删除不必要的代码
	 */
	public void destory() {
		String controllerPath = this.outputDir + StrUtil.SLASH + this.packagePath.replace(".", "/") + "/controller";
		FileUtil.del(controllerPath);
	}

	public void doMpGeneration() {
		AutoGenerator autoGenerator = new AutoGenerator();
		autoGenerator.setGlobalConfig(globalConfig());
		autoGenerator.setDataSource(dataSourceConfig());
		autoGenerator.setStrategy(strategyConfig());
		autoGenerator.setPackageInfo(packageConfig());
		autoGenerator.execute();

		destory();
	}

}

package com.muzhongyun.codegenerator;

import com.muzhongyun.codegenerator.config.CodeGeneratorConfig;

/**
 * 代码生成器
 *
 * @author HeYongSheng
 * @date 2019/05/17 14:33
 */
public class CodeGenerator {

	/**
	 * Mybatis-Plus的代码生成器:
	 * mp的代码生成器可以生成实体,mapper,mapper对应的xml,service
	 */
	public static void main(String[] args) {

		System.out.println("==========================准备生成文件...==========================");
		CodeGeneratorConfig codeGeneratorConfig = new CodeGeneratorConfig();
		codeGeneratorConfig.doMpGeneration();
		System.out.println("==========================文件生成完成！！！==========================");
	}
}
